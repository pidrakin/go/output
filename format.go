package output

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/pidrakin/go/templates"
	"gopkg.in/yaml.v3"
	"io"
	"reflect"
	"strings"
	"unsafe"
)

type FormatType int

const (
	TableFormat      FormatType = 0
	WideTableFormat             = 1
	YamlFormat                  = 2
	JsonFormat                  = 3
	GoTemplateFormat            = 4
)

func Format[T interface{}](writer io.Writer, format FormatType, formatter TableFormatter, template string, t T) error {
	var result string
	var err error
	if format == TableFormat || format == WideTableFormat {
		if result, err = formatTable(t, format == WideTableFormat, template, formatter); err != nil {
			return err
		}
	}
	if format == YamlFormat {
		if result, err = formatYaml(t); err != nil {
			return err
		}
	}
	if format == JsonFormat {
		if result, err = formatJson(t, template == "compact"); err != nil {
			return err
		}
	}
	if format == GoTemplateFormat {
		if result, err = formatGoTemplate(t, template); err != nil {
			return err
		}
	}
	_, err = fmt.Fprintf(writer, result)
	return err
}

func toSlice(s any) []any {
	v := reflect.ValueOf(s)
	if v.Type().Kind() == reflect.Slice {
		var l []any
		for i := 0; i < v.Len(); i++ {
			l = append(l, v.Index(i).Interface())
		}
		return l
	} else {
		return []any{s}
	}
}

func formatTable(t any, wide bool, template string, formatter TableFormatter) (string, error) {
	var customColumns []string
	if template != "" {
		customColumns = strings.Split(template, ":")
	}
	var buf bytes.Buffer
	if err := Table(&buf, toSlice(t), wide, customColumns, formatter); err != nil {
		return "", err
	}
	return buf.String(), nil
}

func formatYaml[T interface{}](t T) (string, error) {
	marshalled, err := yaml.Marshal(t)
	if err != nil {
		return "", err
	}
	return string(marshalled), nil
}

func formatJson[T interface{}](t T, compact bool) (string, error) {
	var err error
	var marshalled []byte
	if compact {
		marshalled, err = json.Marshal(t)
	} else {
		marshalled, err = json.MarshalIndent(t, "", "  ")
	}
	if err != nil {
		return "", err
	}
	return string(marshalled), nil
}

func mapSlice(slice any) []any {
	var result []any
	v := reflect.ValueOf(slice)
	for i := 0; i < v.Len(); i++ {
		result = append(result, mapAny(v.Index(i).Interface()))
	}
	return result
}

func structToMap(obj any) map[string]any {
	v := reflect.ValueOf(obj)
	if !v.CanAddr() {
		v2 := reflect.New(v.Type()).Elem()
		v2.Set(v)
		v = v2
	}
	if v.Type().Kind() == reflect.Pointer {
		v = v.Elem()
	}
	typeOfV := v.Type()

	result := map[string]any{}
	for i := 0; i < v.NumField(); i++ {
		fieldName := typeOfV.Field(i).Name
		value := reflect.NewAt(v.Field(i).Type(), unsafe.Pointer(v.Field(i).UnsafeAddr())).Elem().Interface()
		result[fieldName] = value
	}
	return result
}

func mapStruct(obj any) map[string]any {
	result := map[string]any{}
	for key, value := range structToMap(obj) {
		result[key] = mapAny(value)
	}
	return result
}

func mapAny(obj any) any {
	v := reflect.ValueOf(obj)
	if v.Type().Kind() == reflect.Pointer {
		v = v.Elem()
	}
	if v.Type().Kind() == reflect.Struct {
		return mapStruct(v.Interface())
	}
	if v.Type().Kind() == reflect.Slice {
		return mapSlice(v.Interface())
	}
	return v.Interface()
}

func formatGoTemplate[T interface{}](t T, template string) (string, error) {
	templateData := templates.TemplateData{
		"Root": mapAny(t),
	}
	template = fmt.Sprintf("{{- with .Root -}}%s{{- end -}}", template)
	rendered, err := templates.Tprintf(template, templateData)
	return rendered, err
}
