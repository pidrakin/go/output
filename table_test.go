package output

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

type Row struct {
	NameValue string `yaml:"name-value,omitempty"`
	Value     string
}

func TestTable(t *testing.T) {
	rows := []Row{
		{NameValue: "foo", Value: "bar"},
		{NameValue: "a", Value: "bbbbb"},
	}
	var buf bytes.Buffer
	err := Table(&buf, rows, true, []string{})
	tests.AssertNoError(t, err)

	expected := "NAME_VALUE  VALUE  \nfoo         bar    \na           bbbbb  \n"
	assert.Equal(t, expected, buf.String())
}

func TestTableSkip(t *testing.T) {
	t.Run("one empty", func(t *testing.T) {
		rows := []Row{
			{NameValue: "foo", Value: "bar"},
			{NameValue: "", Value: "bbbbb"},
		}
		var buf bytes.Buffer
		err := Table(&buf, rows, true, []string{})
		tests.AssertNoError(t, err)

		expected := "NAME_VALUE  VALUE  \nfoo         bar    \n            bbbbb  \n"
		assert.Equal(t, expected, buf.String())
	})
	t.Run("all empty", func(t *testing.T) {
		rows := []Row{
			{NameValue: "", Value: "bar"},
			{NameValue: "", Value: "bbbbb"},
		}
		var buf bytes.Buffer
		err := Table(&buf, rows, true, []string{})
		tests.AssertNoError(t, err)

		expected := "VALUE  \nbar    \nbbbbb  \n"
		assert.Equal(t, expected, buf.String())
	})
}
