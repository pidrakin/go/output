package output

import (
	"fmt"
	"io"
	"os"

	"github.com/fatih/color"
	log "github.com/sirupsen/logrus"
)

var (
	Writer   io.Writer = os.Stdout
	ExitFunc           = os.Exit
)

func Successfully(text string) error {
	tick := color.New(color.FgGreen, color.Bold).Sprintf("✔")
	_, err := fmt.Fprintf(Writer, "[ %s ] successfully %s 🎉\n", tick, text)
	return err
}

func Failed(text string, args ...interface{}) error {
	log.Errorf(text, args...)
	tick := color.New(color.FgRed, color.Bold).Sprintf("✘")
	if _, err := fmt.Fprintf(Writer, "[ %s ] failed %s 😭\n", tick, fmt.Sprintf(text, args...)); err != nil {
		return err
	}
	ExitFunc(1)
	return nil
}
