package output

import (
	"fmt"
	"gitlab.com/pidrakin/go/slices"
	"gitlab.com/pidrakin/go/templates"
	"io"
	"reflect"
	"regexp"
	"strings"
	"unsafe"
)

type TableFormatter func(name string, value string) string

type cell struct {
	Value   string
	Padding int
}

func Table[T interface{ any | *any }](writer io.Writer, rows []T, wide bool, customColumns []string, formatter ...TableFormatter) error {

	f := func(name string, value string) string { return value }
	if len(formatter) > 1 {
		return fmt.Errorf("only one formatter is allowed")
	}
	if len(formatter) > 0 && formatter[0] != nil {
		f = formatter[0]
	}

	filterRegex := regexp.MustCompile(`\x1b\[[^m]+m`)

	var colLengths []int
	var header []string
	var rowData [][]cell
	var skip []int

	for idx, row := range rows {
		v := reflect.ValueOf(row)
		if !v.CanAddr() {
			v2 := reflect.New(v.Type()).Elem()
			v2.Set(v)
			v = v2
		}
		if v.Type().Kind() == reflect.Pointer {
			v = v.Elem()
		}
		typeOfS := v.Type()
		rowData = append(rowData, make([]cell, v.NumField()))
		for i := 0; i < v.NumField(); i++ {
			fieldName := templates.ToUpperSnakeCase(typeOfS.Field(i).Name)

			if idx == 0 {
				colLengths = append(colLengths, len(fieldName))
				header = append(header, fieldName)
				if i == 0 {
					skip = make([]int, v.NumField())
				}
			}

			value := reflect.NewAt(v.Field(i).Type(), unsafe.Pointer(v.Field(i).UnsafeAddr())).Elem()
			valueString := fmt.Sprintf("%v", value.Interface())

			if len(customColumns) > 0 {
				if !slices.Contains(slices.Map(customColumns, templates.ToUpperSnakeCase), fieldName) {
					skip[i]++
				}
			} else if valueString == "" && strings.Contains(typeOfS.Field(i).Tag.Get("yaml"), "omitempty") {
				skip[i]++
			} else if !wide {
				if _, ok := typeOfS.Field(i).Tag.Lookup("wide"); ok {
					skip[i]++
				}
			}

			formatted := f(fieldName, valueString)
			filtered := filterRegex.ReplaceAllString(formatted, "")
			if colLengths[i] < len(filtered) {
				colLengths[i] = len(filtered)
			}
			rowData[idx][i] = cell{Value: formatted, Padding: len(formatted) - len(filtered)}
		}
	}
	data := templates.TemplateData{
		"colLengths": colLengths,
		"header":     header,
		"rowData":    rowData,
		"skip":       skip,
	}

	tmpl := `
{{- $colLengths := .colLengths }}
{{- $nrRows := len .rowData }}
{{- $skip := .skip }}
{{- range $idx, $headerName := .header -}}
    {{- if (lt (index $skip $idx) $nrRows) -}}
        {{ $headerName | printf (printf "%%-%ds  " (index $colLengths $idx)) }}
    {{- end -}}
{{- end }}
{{ range $idx, $row := .rowData -}}
	{{- range $colIdx, $cell := $row -}}
        {{- if (lt (index $skip $colIdx) $nrRows) -}}
			{{ $cell.Value | printf (printf "%%-%ds  " (add (index $colLengths $colIdx) $cell.Padding)) }}
        {{- end -}}
    {{- end }}
{{ end -}}
`
	parsed, err := templates.Tprintf(tmpl, data)
	if err != nil {
		return err
	}
	_, err = fmt.Fprintf(writer, parsed)
	return err
}
