package output

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

type Sub struct {
	Prop string
}

type Property struct {
	Key          string
	Slice        []int
	Structs      []*Sub
	SliceOfSlice [][]int
}

type Object struct {
	Prop1 string
	Prop2 *Property
}

var obj = &Object{
	Prop1: "value",
	Prop2: &Property{
		Key:   "prop2-Key",
		Slice: []int{1, 2},
		Structs: []*Sub{
			{Prop: "foobar"},
		},
		SliceOfSlice: [][]int{{3}, {4}},
	},
}

func Test_formatTable(t *testing.T) {
	output, err := formatTable(obj, true, "", nil)
	tests.AssertNoError(t, err)
	assert.Contains(t, output, "PROP1  PROP2")
	assert.Contains(t, output, "\nvalue  &{prop2-Key [1 2] [")
	assert.Contains(t, output, "] [[3] [4]]}  \n")
}

func Test_formatYaml(t *testing.T) {
	output, err := formatYaml(obj)
	tests.AssertNoError(t, err)
	assert.Equal(t, "prop1: value\nprop2:\n    key: prop2-Key\n    slice:\n        - 1\n        - 2\n    structs:\n        - prop: foobar\n    sliceofslice:\n        - - 3\n        - - 4\n", output)
}

func Test_formatGoTemplate(t *testing.T) {
	output, err := formatGoTemplate(obj, "{{ range $i, $v := . }}{{ $i }}:\t{{ $v }}\n{{ end }}")
	tests.AssertNoError(t, err)
	assert.Equal(t, "Prop1:\tvalue\nProp2:\tmap[Key:prop2-Key Slice:[1 2] SliceOfSlice:[[3] [4]] Structs:[map[Prop:foobar]]]\n", output)
}
