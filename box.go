package output

import (
	"gitlab.com/pidrakin/go/slices"
	"strings"
)

func Box(text string) string {
	var (
		result string
		max    int
	)
	text = strings.ReplaceAll(text, "\t", "  ")
	lines := strings.Split(text, "\n")
	lengths := slices.Map(lines, func(l string) int { return len(l) })
	max = slices.Max(lengths)

	result = "+" + strings.Repeat("=", max+2) + "+"
	for _, line := range lines {
		result += "\n| " + line + strings.Repeat(" ", max-len(line)) + " |"
	}
	result += "\n+" + strings.Repeat("=", max+2) + "+"
	return result
}
