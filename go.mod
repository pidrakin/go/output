module gitlab.com/pidrakin/go/output

go 1.19

require (
	github.com/fatih/color v1.16.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.8.1
	gitlab.com/pidrakin/go/slices v0.0.3
	gitlab.com/pidrakin/go/templates v0.0.2
	gitlab.com/pidrakin/go/tests v0.0.2
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/exp v0.0.0-20240222234643-814bf88cf225 // indirect
	golang.org/x/sys v0.17.0 // indirect
)
