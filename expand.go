package output

import (
	"github.com/mitchellh/go-homedir"
	"strings"
)

func Expand(path string) string {
	expanded, err := homedir.Expand(path)
	if err != nil {
		return path
	}
	home, err := homedir.Dir()
	if err != nil {
		return path
	}
	replacer := strings.NewReplacer("$HOME", home, "${HOME}", home)
	expanded = replacer.Replace(expanded)
	return expanded
}
